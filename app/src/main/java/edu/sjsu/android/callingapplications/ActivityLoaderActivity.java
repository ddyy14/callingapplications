package edu.sjsu.android.callingapplications;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;


public class ActivityLoaderActivity extends AppCompatActivity {


   private Button callButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        callButton = findViewById(R.id.call_button);
        callButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phoneNumber = "tel: +19491234444";
                Intent myActivity = new Intent(Intent.ACTION_DIAL, Uri.parse(phoneNumber));
                startActivity(myActivity);
            }
        });

    }
    public void webButton(View view) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.amazon.com"));
        Intent chooser = Intent.createChooser(intent, "Load http://www.google.com with: ");
        startActivity(chooser);
    }
}
